<?php

class SQLHandle{
    private $servername;
    private $username;
    private $password;
    private $dbname;
    private $conn;

    private $tableID;

    private $pageLoginTableID;
    private $adminLoginTableID;
    private $tokenTableID;
    #region Construct and connect
    
    public function __construct($tableID = 0){
        if (file_exists(__DIR__ . '/database_config.php')) {
            $config = include __DIR__ . '/database_config.php';
        }
        $this->servername = $config['servername'];
        $this->username = $config['username'];
        $this->password = $config['password'];
        $this->dbname = $config['dbname'];

        $this->UpdateDatabaseID($tableID);
    }

    public function UpdateDatabaseID($tableID){
        $this->tableID = $tableID;
        $this->pageLoginTableID = "PAGELOGININFO";
        $this->adminLoginTableID = "ADMINLOGININFO";
        $this->tokenTableID = "TOKEN";

    }


    function Connect(){
        $this->conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
            return false;
        }
        $this->conn->set_charset('utf8');
        return true;
    }
    function QueryData($sql){
        $result = $this->conn->query($sql);
        var_dump("<br>");
        var_dump($sql);
        var_dump("<br>");
        var_dump("Kết quả trả về: ");
        var_dump($this->conn->error);
        var_dump("<br>");
        return $result;
    }
    function CloseConnect(){
        $this->conn->close();
    }
    function SQLDataQuery($sql){
        $this->Connect();
        $result = $this->QueryData($sql);
        $this->CloseConnect();
        return $result;
    }
    #endregion

    #region Userdata session
    public function GetUserData($userID){
        $tableName = $this->userDataTableID;
        $sql = "SELECT * FROM  $tableName WHERE ID = $userID";
        $result = $this->SQLDataQuery($sql);

        $UserData = CreateNewUserDataEntity();
        if ($result){
            $row = $result->fetch_assoc();
            $UserData["ID"] =  $row["ID"];
            $UserData["Address"] =  $row["USERADDRESS"];
            $UserData["Phone"] =  $row["USERPHONE"];
            $UserData["Email"] =  $row["USEREMAIL"];
            $UserData["Status"] =  json_decode($row["USERSTATUS"]);
            $UserData["Temp"] =  json_decode($row["USERTEMP"],true);
            $UserData["Order"] =  json_decode($row["USERORDER"],true);
            $UserData["Command"] =  $row["USERCOMMAND"];
        }
        return $UserData;
    }
    public function UpdateUserData($UserData){
        $tableName = $this->userDataTableID;
        $sql = "UPDATE $tableName
                SET USERADDRESS = ".json_encode($UserData['Address'],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES).
                ", USERPHONE = ". json_encode($UserData['Phone']).
                ", USEREMAIL = ". json_encode($UserData['Email']).
                ", USERSTATUS = ". json_encode($UserData['Status']).
                ", USERTEMP = ". $this->EncodeToString($UserData["Temp"]).
                ", USERORDER = ". $this->EncodeToString($UserData["Order"]).
                ", USERCOMMAND = ". json_encode($UserData["Command"]).
                 " WHERE ID = ". $UserData['ID'];
        $this->SQLDataQuery($sql);
    }
    public function WriteNewUserData($userID){
        $tableName = $this->userDataTableID;
        $UserData = CreateNewUserDataEntity();
        $UserData['ID']= $userID;
        $sql = "INSERT INTO $tableName VALUES ($userID, '', '', '', 1, '', '', 0)";
        $this->SQLDataQuery($sql);
        return $UserData;
    }

    public function DeleteUserData($UserID){
        $tableName = $this->userDataTableID;
        $sql = "DELETE FROM $tableName WHERE ID = ". $UserID;
        $this->SQLDataQuery($sql);
    }
    #endregion
    
    #region User order

    public function WriteOrder($UserData,$UserName){
        $tableName = $this->userOrderTableID;
        $UserID = $UserData['ID'];
        $UserName = json_encode($UserName,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $order = $this->EncodeToString($UserData["Order"]);
        $address =json_encode($UserData['Address'],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $phone = json_encode($UserData['Phone']);

        $today = new DateTime("now", new DateTimeZone('Asia/Saigon'));
        $orderDate = $today->format("Y-m-d H:i:s");
    
        $sql = "INSERT INTO $tableName (USERID,USERNAME,USERORDER,USERPHONE,USERADDRESS,ORDERDAY,CONFIRM,DELIVERIED)
                VALUES ($UserID,$UserName, $order, $phone, $address, $orderDate, 0, 0)";
        $this->SQLDataQuery($sql);
        return $UserData;
    }

    public function GetAllOrder(){
        $tableName = $this->userOrderTableID;
        $sql = "SELECT * FROM $tableName";
        $result = $this->SQLDataQuery($sql);
        $listOrder = [];
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $order = CreateNewOrderDataEntity();
                $order['ID'] = $row['ID'];
                $order['UserID'] = $row['USERID'];
                $order['Name'] = $row['USERNAME'];
                $order['Phone'] = $row['USERPHONE'];
                $order['Address'] = $row['USERADDRESS'];
                $order['Order'] = $this->DecodeFromString($row['USERORDER']);
                $order['OrderDay'] = $row['ORDERDAY'];
                $order['Confirm'] = $row['COMFIRM'];
                $order['Deliveried'] = $row['DELIVERIED'];
                $listOrder[] = $order;
            }
        }
        return $listOrder;
    }

    public function GetOrder($id){
        $tableName = $this->userOrderTableID;
        $sql = "SELECT * FROM $tableName WHERE ID = $id";
        $result = $this->SQLDataQuery($sql);
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $order = CreateNewOrderDataEntity();
            $order['ID'] = $row['ID'];
            $order['UserID'] = $row['USERID'];
            $order['Name'] = $row['USERNAME'];
            $order['Phone'] = $row['USERPHONE'];
            $order['Address'] = $row['USERADDRESS'];
            $order['Order'] = $this->DecodeFromString($row['USERORDER']);
            $order['OrderDay'] = $row['ORDERDAY'];
            $order['Confirm'] = $row['COMFIRM'];
            $order['Deliveried'] = $row['DELIVERIED'];
        }
        return $order;
    }
    public function UpdateOrder($order){
        $tableName = $this->userOrderTableID;
        $sql = "UPDATE $tableName
                SET USERID = ".json_encode($order['UserID']).
                ", USERNAME = ". json_encode($order['Name'],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES).
                ", USERPHONE = ". json_encode($order['Phone']).
                ", USERADDRESS = ". json_encode($order['Address'],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES).
                ", USERORDER = ". $this->EncodeToString($order["Order"]).
                ", ORDERDAY = ". $order["OrderDay"].
                ", COMFIRM = ". $order["Confirm"].
                ", DELIVERIED = ". $order["Deliveried"].
                 " WHERE ID = ". $order['ID'];
        $this->SQLDataQuery($sql);
    }

    #endregion

    #region Product data
    public function GetProducts(){
        $tableName = $this->productTableID;
        $sql = "SELECT * FROM $tableName";
        $result = $this->SQLDataQuery($sql);
        $listProduct = [];
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $product = CreateNewProductDataEntity();
                $product['ID'] = $row['ID'];
                $product['Name'] = $row['PRODUCTNAME'];
                $product['Price'] = $row['PRODUCTPRICE'];
                $product['Quantity'] = $row['PRODUCTQUANTITY'];
                $product['Image'] = $row['PRODUCTIMAGE'];
                $product['Description'] = $row['PRODUCTDESCRIPTION'];
                $listProduct[] = $product;
            }
        }
        return $listProduct;
    }
    public function GetProductWithID($ID){
        $tableName = $this->productTableID;
        $sql = "SELECT * FROM $tableName WHERE ID = $ID";
        $result = $this->SQLDataQuery($sql);
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $product = CreateNewProductDataEntity();
            $product['ID'] = $row['ID'];
            $product['Name'] = $row['PRODUCTNAME'];
            $product['Price'] = $this->DecodeFromString($row['PRODUCTPRICE']);
            $product['Image'] = $row['PRODUCTIMAGE'];
            $product['Description'] = $row['PRODUCTDESCRIPTION'];
        }
        return $product;
    }

    public function ImportNewProduct($product){
        $tableName = $this->productTableID;
        $productName = json_encode($product["Name"],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $price =  $this->EncodeToString($product['Price']).
        $image = json_encode($product['Image'],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $description = json_encode($product["Description"],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $sql = "INSERT INTO $tableName (PRODUCTNAME,PRODUCTPRICE,PRODUCTIMAGE,PRODUCTDESCRIPTION)
                 VALUES ($productName, $price, $image, $description)";
        $this->SQLDataQuery($sql);
        return $product;
    }
    public function UpdateProduct($product){
        $tableName = $this->productTableID;
        $sql = "UPDATE $tableName
                SET PRODUCTNAME = ".json_encode($product['Name'],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES).
                ", PRODUCTPRICE = ". $this->EncodeToString($product['Price']).
                ", PRODUCTIMAGE = ". $product['Image'].
                ", PRODUCTDESCRIPTION = ". json_encode($product["Description"],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES).
                 " WHERE ID = ". $product['ID'];
        $this->SQLDataQuery($sql);
    }
    #endregion

    #region Logs data

    public function WriteLogs($action, $message){
        $today = new DateTime("now", new DateTimeZone('Asia/Saigon'));
        $time  = $today->format("Y-m-d H:i:s");
        $id = $this->tableID;
        $message = json_encode($message,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $sql = "INSERT INTO LOGS (TIME,USERID,ACTION,MESSAGE)
                VALUES ('$time','$id','$action','$message')";
        $result = $this->SQLDataQuery($sql);
    }

    public function GetLogs(){
        $sql = "SELECT * FROM LOGS";
        $result = $this->SQLDataQuery($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                echo "<br>". $row["TIME"]." ". $row["USERID"]. " ". $row["ACTION"]. " ". $row["MESSAGE"]."<br>";
            }
        } else {
            echo "Log empty. <br>";
        }
    }

    public function DeleteLogs(){
        $sql = "DELETE FROM LOGS";
        $this->SQLDataQuery($sql);
    }
    #endregion

    #region Account page and admin

    public function LoginPageAccount($userID){
        $tableName = $this->pageLoginTableID;
        $sql = "SELECT * FROM $tableName WHERE USERID = '$userID'";
        $result = $this->SQLDataQuery($sql);
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            return $row;
        }
        return false;
    }

    public function CreateNewPageAccount($userID,$userToken,$pageID,$email){
        $tableName = $this->pageLoginTableID;
        $sql = "INSERT INTO $tableName (USERID,USERTOKEN,PAGEID,EMAIL)
                VALUES ('$userID', '$userToken', '$pageID','$email')";
        return $this->SQLDataQuery($sql);
    }

    public function LoginAdminAccount($username,$password){
        $tableName = $this->adminLoginTableID;
        $sql = "SELECT * FROM $tableName WHERE USERNAME = '$username'";
        $result = $this->SQLDataQuery($sql);
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            if (password_verify($password,$row['PASSWORD'])){
                return $username;
            }
        }
        return false;
    }

    public function CreateNewAdminAccount($username,$password){
        $tableName = $this->adminLoginTableID;
        $username = json_encode($username);
        $password = json_encode(password_hash($password,PASSWORD_DEFAULT));
        $sql = "INSERT INTO $tableName (USERNAME,PASSWORD)
                VALUES ($username, $password)";
        return $this->SQLDataQuery($sql);
    }

    #endregion

    #region Tokken
    public function GetToken($pageID){
        $tableName = $this->tokenTableID;
        $sql = "SELECT * FROM $tableName WHERE PAGEID = $pageID";
        $result = $this->SQLDataQuery($sql);
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            if($pageID == $row['PAGEID']){
                return $row['TOKEN'];
            }
        }
        return false;
    }

    public function UpdateToken($pageID,$token){
        $tableName = $this->tokenTableID;
        $sql = "UPDATE $tableName
                SET TOKEN = ".json_encode($token).
                 " WHERE PAGEID = ".$pageID;
        return $this->SQLDataQuery($sql);
    }

    public function AddNewToken($pageID,$token){
        $tableName = $this->tokenTableID;
        $sql = "INSERT INTO $tableName (PAGEID,TOKEN)
                VALUES ('$pageID', '$token')";
        return $this->SQLDataQuery($sql);
    }
    #endregion

    #region Command action

    public function GetCommandScript($stepID){
        $tableName = $this->commandTableID;
        $sql = "SELECT * FROM $tableName WHERE STEP = $stepID";
        $result = $this->SQLDataQuery($sql);
        if ($result){
            $row = $result->fetch_assoc();
            $command =  json_decode($row["ACTION"],true);
        }
        return $command;

    }

    public function UpdateCommandScript($stepID,$command){
        $tableName = $this->commandTableID;
        $command = $this->EncodeToString($command);
        $sql = "UPDATE $tableName
                SET ACTION = ".$command." WHERE STEP = ". $stepID;
        $this->SQLDataQuery($sql);

    }

    public function WriteNewCommandScript($userID,$command){
        $tableName = $this->commandTableID;
        $command = $this->EncodeToString($command);
        $sql = "INSERT INTO $tableName (STEP,ACTION)
                VALUES ($userID, $command)";
        return $this->SQLDataQuery($sql);
    }
    
    #endregion



    #region Create new database

    public function CreateInitDatabase(){
        $this->Connect();
        $this-> CreateLoginPageDatabase();
        $this-> CreateTokenStoredDatabase();
        $this-> CreateLoginAdminDatabase();
        $this-> CreateLogTable();
        $this->CloseConnect();
    }

    public function CreateNewCustomerDatabase($tableID){
        $this->UpdateDatabaseID($tableID);
        $this->Connect();
        $this->CreateNewUserDataTable();
        $this->CreateNewUserOrderTable();
        $this->CreateNewProductTable();
        $this->CreateCommandTable();
        $this->CloseConnect();
    }


    function CreateLogTable(){
        $tableName = "LOGS";
        $sql = "CREATE TABLE $tableName (
            ID int NOT NULL AUTO_INCREMENT,
            TIME datetime,
            USERID varchar(255),
            ACTION varchar(255),
            MESSAGE varchar(255),
            PRIMARY KEY (ID)
        )";
        $this->QueryData($sql);
    }

    function CreateLoginPageDatabase(){
        $tableName = $this->pageLoginTableID;
        $sql = "CREATE TABLE $tableName (
            USERID varchar(255),
            USERTOKEN varchar(255),
            PAGEID varchar(255),
            EMAIL varchar(255),
            PRIMARY KEY (USERID)
        )";
        $this->QueryData($sql);
    }

    function CreateTokenStoredDatabase(){
        $tableName = $this->tokenTableID;
        $sql = "CREATE TABLE $tableName (
            PAGEID varchar(255),
            TOKEN varchar(255),
            PRIMARY KEY (PAGEID)
        )";
        $this->QueryData($sql);
    }

    function CreateLoginAdminDatabase(){
        $tableName = $this->adminLoginTableID;
        $sql = "CREATE TABLE $tableName (
            USERNAME varchar(255),
            PASSWORD varchar(255),
            PRIMARY KEY (USERNAME)
        )";
        $this->QueryData($sql);
    }
  
    function CreateNewUserDataTable(){
        $tableName = $this->userDataTableID;
        $sql = "CREATE TABLE $tableName (
            ID varchar(20),
            USERADDRESS varchar(255),
            USERPHONE varchar(20),
            USEREMAIL varchar(255),
            USERSTATUS int,
            USERTEMP text,
            USERORDER text,
            USERCOMMAND int,
            PRIMARY KEY (ID)
        )";
        $result = $this->QueryData($sql);
    }
    function CreateNewUserOrderTable(){
        $tableName = $this->userOrderTableID;
        $sql = "CREATE TABLE $tableName (
            ID int NOT NULL AUTO_INCREMENT,
            USERID varchar(255),
            USERNAME varchar(20),
            USERORDER text,
            USERPHONE varchar(20),
            USERADDRESS varchar(255),
            ORDERDAY datetime,
            CONFIRM int,
            DELIVERIED int,
            PRIMARY KEY (ID)
        )";
        $result = $this->QueryData($sql);
    }
    function CreateNewProductTable(){
        $tableName = $this->productTableID;
        $sql = "CREATE TABLE $tableName (
            ID int NOT NULL AUTO_INCREMENT,
            PRODUCTNAME varchar(255),
            PRODUCTPRICE text,
            PRODUCTIMAGE varchar(255),
            PRODUCTDESCRIPTION text,
            PRIMARY KEY (ID)
        )";
        $result = $this->QueryData($sql);
    }
    function CreateCommandTable(){
        $tableName = $this->commandTableID;
        $sql = "CREATE TABLE $tableName (
            STEP int,
            ACTION text,
            PRIMARY KEY (STEP)
        )";
        $result = $this->QueryData($sql);
    }
    #endregion

    #region Support function
    function EncodeToString($item){
        $item = json_encode($item,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        return json_encode($item,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
    }

    function DecodeFromString($item){
        $item = json_decode($item);
        return json_decode($item);
    }

    public function DeleteTable($tableName){
        $sql = "DELETE FROM  $tableName";
        $this->SQLDataQuery($sql);
    }
    #endregion
}
?>
