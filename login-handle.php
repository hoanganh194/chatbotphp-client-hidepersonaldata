<?php 
session_start();
if (file_exists(__DIR__ . '/config.php')) {
    $config = include __DIR__ . '/config.php';
}

if (!isset($_SESSION['username'])) {
    header('Location: ' .  $config['url']. '/dangnhap.php');
    exit;
}
?>