var ItemIdReceiver = 0;
var pageId;
function GetFixItem()
{
    var IdFixURL = document.URL;
    var IdFix = new URL(IdFixURL);
    var IdScript = IdFix.searchParams.get("IdFix");
    pageId = IdFix.searchParams.get("PageId");
    console.log(pageId);
    FixItem(IdScript);
}
function FixItem(IdScript) {
    var scriptID = IdScript;
    var url = "https://condieuchatbot.xyz/getproduct";
    var menuId = $("ul.nav").first().attr("id");
    var request = $.ajax({
        url: url,
        type: "POST",
        data: { 
            pageid: pageId,
            itemid: scriptID
        },
        dataType: "html",
        success: function(data) {
            var DataIncomeObj = JSON.parse(data);
            ItemIdReceiver = DataIncomeObj.item.ID;
            FixItemhtml(DataIncomeObj.item)
        },
        error: function(request, error) {
            alert("Request: " + JSON.stringify(request));
        }
    });

    request.done(function(msg) {
        $("#log").html(msg);
    });

    request.fail(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}

function FixItemhtml(DataIncomeObj)
{
    var FixHtml = 
    `
        <div class="container">
            <div class="col-lg-6 col-md-6">
                <div class="login_part_form">
                    <div class="login_part_form_iner">
                        <h3><br> Xin hãy nhập thông tin sản phẩm mới:</h3>
                        <!-- <form class="row contact_form" action="/ThemSanPham" method="POST" enctype="multipart/form-data"> -->
                            <div class="col-md-12 form-group p_star">
                                <label>Nhập tên sản phẩm:</label>
                                <input type="text" class="form-control" id="ItemName" name="name" value="${DataIncomeObj.Name}" placeholder="Tên sản phẩm" required>
                            </div>
                            <div class="col-md-12 form-group p_star">
                                <button onclick="Addtype()" style="color: red;">Add Type</button>
                                <div id="AddtypeHTML"></div>
                            </div>
                            <div class="col-md-12 form-group p_star">
                                <label>Nhập thông tin sản phẩm:</label>
                                <textarea class="form-control" id="ItemInfo" name="thongtin" cols="30" rows="10" value="">${DataIncomeObj.Description}</textarea>
                            </div>
                            <div class="col-md-12 form-group p_star">
                                <label>Chọn hình ảnh (5mb tối đa, file png):</label>
                                <input type="text" class="form-control" id="ItemImage" name="hinhanh" value="${DataIncomeObj.Image}">
                            </div>
                            <div class="col-md-12 form-group">
                            <button onclick="SubmitProduct()" class="btn_3">
                                Xác nhận
                            </button>
                            </div>
                        <!-- </form> -->
                    </div>
                </div>
            </div>
        </div>
    `
    document.getElementById("Fixing-shower").innerHTML = FixHtml;
    console.log(DataIncomeObj);
    if(DataIncomeObj.Price != null)
    {
        for(var i = 0; i < DataIncomeObj.Price.length; i++)
        {
            AddtypeWithKichBan(DataIncomeObj.Price[i],i);
        }
    }

}
let DeletedNameType = [];
let index=0;
let arrayNameType = [];
function AddtypeWithKichBan(DataIncomeObj,i) {
        var scriptHTML = CreateAddNameTypeHTMLWithKichBan(DataIncomeObj,i);
        var MainDiv = document.createElement('div');
        MainDiv.id = `AddtypeHTML${index}`;
        MainDiv.innerHTML = scriptHTML;  
        document.getElementById("AddtypeHTML").appendChild(MainDiv);
        arrayNameType.push(i);
        index += 1;
}

function CreateAddNameTypeHTMLWithKichBan(DataIncomeObj,elementID){
    console.log(DataIncomeObj);
    var htmlString =  `
        <p>Đơn giá: 
            <input type="text" id="Name${DataIncomeObj.ID}" value="${DataIncomeObj.NameType}"></input>
            <input type="number" id="Value${DataIncomeObj.ID}" value="${DataIncomeObj.Price}"></input>
            <button onclick = "DeleteNameType(${elementID})" style="color:red;">Delete</button>
        </p>`;
    return  htmlString;
}

function DeleteNameType(elementID)
{
    DeletedNameType.push(elementID);
    document.getElementById(`AddtypeHTML${elementID}`).innerHTML="";
}
function Addtype() {
    var scriptHTML = CreateAddNameTypeHTML(index);
    var MainDiv = document.createElement('div');
    MainDiv.id = `AddtypeHTML${index}`;
    MainDiv.innerHTML = scriptHTML;
    var innerHTML = CreateAddNameTypeHTML(index);   
    document.getElementById("AddtypeHTML").appendChild(MainDiv);
    arrayNameType.push(index);
    index += 1;
}
function CreateAddNameTypeHTML(elementID){
    var htmlString =  `
        <p>Đơn giá: 
            <input type="text" id="Name${elementID}"></input>
            <input type="number" id="Value${elementID}"></input>
            <button onclick = "DeleteNameType(${elementID})" style="color:red; >Delete</button>
        </p>`;
    return  htmlString;
}
function SubmitProduct(){
    var itemName = document.getElementById("ItemName").value;
    var itemDescription = document.getElementById("ItemInfo").value;
    var itemImageUrl = document.getElementById("ItemImage").value;
    let summitedNameType = [];
    for(var indexer of arrayNameType)
    {
        if(DeletedNameType.find(x=>x == indexer) == null)
        {
            var nameType = GetNameTypePrice(indexer,`Name${indexer}`,`Value${indexer}`);
            summitedNameType.push(nameType);
        } 
    }
    SubmitHander(itemName,itemDescription,itemImageUrl,summitedNameType);
}

function GetNameTypePrice(elementID,elementPriceName,elementPriceValue){
    var nameTypeID = elementID;
    var nameTypeName = document.getElementById(elementPriceName).value;
    var nameTypePrice = document.getElementById(elementPriceValue).value;
    var nameType = {
        id : nameTypeID,
        name : nameTypeName,
        price : nameTypePrice
    }
    return nameType;
}

function SubmitHander(itemName,itemDescription,itemImageUrl,summitedNameType)
{
    var dataObject =  CreateProductDataObject(ItemIdReceiver,itemName,itemDescription,itemImageUrl,summitedNameType);
    console.log(dataObject);
    var url = "https://condieuchatbot.xyz/updateproduct";
    var menuId = $("ul.nav").first().attr("id");
    var request = $.ajax({
    url: url,
    type: "POST",
    data: {
        pageid: pageId,
        itemid: ItemIdReceiver,
        item : dataObject
    },
    dataType: "html"
    });

    request.done(function(msg) {
    $("#log").html( msg );
    });

    request.fail(function(jqXHR, textStatus) {
    alert( "Request failed: " + textStatus );
    });
    window.open("index.php");
}

function CreateProductDataObject(itemID,itemName,itemDescription,itemImageUrl,summitedNameType){
    var data = {
        ID: itemID,
        Name: itemName,
        Price: [],
        Image: itemImageUrl,
        Description: itemDescription,
        Feature: true
    }
    for(var nameType of summitedNameType){
        var namePriceObject = {
                ID: nameType.id,
                NameType: nameType.name,
                Price: nameType.price
        };
        data.Price.push(namePriceObject);
    }
    return data;
}