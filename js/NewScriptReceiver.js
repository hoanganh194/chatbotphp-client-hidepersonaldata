let DeletedDirect = [];
let indexScript = 0;
let indexActionList = [];
let hashMap = [];

var pageId;

function GetListScript(pageID)
{
    pageId = pageID;
    var scriptID = "1";
    var url = "https://e9ba-103-245-246-232.ngrok.io/getlistscriptid";
    var menuId = $("ul.nav").first().attr("id");
    var request = $.ajax({
        url: url,
        type: "POST",
        data: { 
            pageid: pageId
        },
        dataType: "html",
        success: function(data) {
            console.log(data);
            var JsonData = JSON.parse(data);
            console.log(JsonData);
            ShowListOfScriptID(JsonData);
        },
        error: function(request, error) {
            alert("Request: " + JSON.stringify(request));
        }
    });

    request.done(function(msg) {
        $("#log").html(msg);
    });

    request.fail(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}

function ShowListOfScriptID(JsonData)
{
    var ListOfScriptData = JsonData.item;
    for(var i = 0; i < ListOfScriptData.length; i++)
    {
        if(ListOfScriptData[i] != "")
        {
            htmlString = 
            `
                <tr>
                    <td>
                        <p>Kịch Bản: ${ListOfScriptData[i]}</p>
                    </td>
                    <td>
                        <button class="btn_3 p-4" style="color:red;" onclick="DeleteScript(${ListOfScriptData[i]})"">
                            Xóa kịch bản
                        </button>
                    </td>
                    <td>
                        <form action="editkichban.php" method="GET">
                        <input type="hidden" name="IdScript" value="${ListOfScriptData[i]}">
                            <input type="submit" id="FixItemInfo${i}" class="btn_3 p-4" role="button" value="Xem Thêm ">
                        </form>
                    </td>
                </tr>
            `
            document.getElementById("ShowListKichBan").innerHTML +=htmlString;
        }
    }
}
function DeleteScript(DeleteScript)
{
    var ObjectScript = DeleteScript;
    console.log(ObjectScript);
    var url = "https://e9ba-103-245-246-232.ngrok.io/deletescript";
    var menuId = $("ul.nav").first().attr("id");
    var request = $.ajax({
        url: url,
        type: "POST",
        data: {
            pageid: pageId,
            itemid: ObjectScript, 
        },
        dataType: "html"
        });

        request.done(function(msg) {
        $("#log").html( msg );
        });
        request.fail(function(jqXHR, textStatus) {
        alert( "Request failed: " + textStatus );
        });
}
function DeleteDirect(indexOfDirect){
    DeletedDirect.push(indexOfDirect);
    document.getElementById(`MainDiv${indexOfDirect}`).innerHTML = "";
}
function GetFixItem()
{
    var IdFixURL = document.URL;
    var IdFix = new URL(IdFixURL);
    var IdScript = IdFix.searchParams.get("IdScript");
    console.log(IdScript);
    SubmitHanderGetData(IdScript);
}
function SubmitHanderGetData(IdScript) {
    document.getElementById("IDStatus").value = IdScript;
    EditingID = document.getElementById("IDStatus").value
    var scriptID = IdScript;
    var url = "https://e9ba-103-245-246-232.ngrok.io/getscript";
    var menuId = $("ul.nav").first().attr("id");
    var request = $.ajax({
        url: url,
        type: "POST",
        data: { 
            pageid: pageId,
            itemid: scriptID,
        },
        dataType: "html",
        success: function(data) {
            console.log(data);
            var JsonData = JSON.parse(data);
            console.log(JsonData);
            ShowAll(JsonData);
        },
        error: function(request, error) {
            alert("Request: " + JSON.stringify(request));
        }
    });

    request.done(function(msg) {
        $("#log").html(msg);
    });

    request.fail(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}
function CreateMoreDirect()
{
        var scriptHTML = CreateConditionHTML(indexScript);
        indexActionList.push(0);
        var MainDiv = document.createElement('div');
        MainDiv.id = `MainDiv${indexScript}`;
        MainDiv.innerHTML = scriptHTML;
        document.getElementById("ScriptHTML").appendChild(MainDiv);
        indexOfAction=0;
        indexScript+=1;
}
function CreateConditionHTML(indexOfDirect){
    var htmlString = `
        <input type="text" style="margin-top:2rem" id="TextCheck${indexOfDirect}" placeholder="TextCheck"></input>
        <select  id="TextCheckType${indexOfDirect}">
            <option value="-1">Không Giống</option>
            <option value="0">Không Kiểm Tra</option>
            <option value="1">Giống</option>
        </select>
        <button id="Delete${indexOfDirect}" onclick="DeleteDirect(${indexOfDirect})">Delete Direct</button>
        <br>
        <input type="text" class="" id="PayloadCheck${indexOfDirect}" placeholder="PayloadCheck"></input>
        <select id="PayloadCheckType${indexOfDirect}">
            <option value="-1">Không Giống</option>
            <option value="0">Không Kiểm Tra</option>
            <option value="1">Giống</option>
        </select>
        <br>
        <input type="text" class="" id="QuickMessageCheck${indexOfDirect}" placeholder="QuickMessageCheck"></input>
        <select id="QuickCheckType${indexOfDirect}">
            <option value="-1">Không Giống</option>
            <option value="0">Không Kiểm Tra</option>
            <option value="1">Giống</option>
        </select>
        <br>
        <button class="btn_4" onclick="CreateActionDetailHTML(${indexOfDirect})" id="ActionButton${indexOfDirect}">Thêm action</button>
        <div id="Action${indexOfDirect}"></div>
        `;
    return htmlString;
}
function ShowAll(data)
{
    var scriptHTML = "";
    document.getElementById("ScriptHTML").innerHTML = scriptHTML;
    var KichBan = data.item;
    for(var i = 0; i < KichBan.length; i++)
    {    
        CreateMoreDirectWithKichBan(KichBan);
        if(KichBan[i].Action != null)
        {
            for(var j = 0; j < KichBan[i].Action.length; j++)
            {
                CreateActionDetailHTMLWithKichBan(i,KichBan[i].Action[j]);
                CheckAndCreateActionOptionWithKichBan(i,j,KichBan[i].Action[j]); 
                
                if(KichBan[i].Action[j].Content == "StructureMess" || KichBan[i].Action[j].Content == "QuickMess")
                {  
                    if(KichBan[i].Action[j].Option != null)
                    {
                        for(var k = 0; k < KichBan[i].Action[j].Option.length; k++)
                        {
                            OptionCreatorWithKichBan(i,j,KichBan[i].Action[j].Option[k]);
                        }
                    }
                    
                }
            }
        }
    }
    
}

function CheckToText(Check)
{
    if(Check == 0)
    {
        return "Không Kiểm Tra";
    }
    if(Check == -1)
    {
        return "Không Giống";
    }
    if(Check == 1)
    {
        return "Giống";
    }
}

function CreateMoreDirectWithKichBan(KichBan){
    var scriptHTML = CreateConditionHTMLWithKichBan(indexScript,KichBan);
    indexActionList.push(0);
    var MainDiv = document.createElement('div');
    MainDiv.id = `MainDiv${indexScript}`;
    MainDiv.innerHTML = scriptHTML;
    document.getElementById("ScriptHTML").appendChild(MainDiv);
    indexOfAction=0;
    indexScript+=1;
}
function CreateConditionHTMLWithKichBan(indexOfDirect,KichBan){
    var TextCheckTypeReplace = CheckToText(KichBan[indexOfDirect].TextCheckType);
    var PayloadCheckReplace = CheckToText(KichBan[indexOfDirect].PayloadCheckType);
    var QuickCheckTypeReplace = CheckToText(KichBan[indexOfDirect].QuickCheckType);
    var htmlString = `
        <input type="text" style="margin-top:2rem" id="TextCheck${indexOfDirect}" placeholder="TextCheck" value="${KichBan[indexOfDirect].TextCheck}"></input>
        <select  id="TextCheckType${indexOfDirect}">
            <option selected="selected" value="${KichBan[indexOfDirect].TextCheckType}" style="color:red;">${TextCheckTypeReplace}</option>
            <option value="-1">Không Giống</option>
            <option value="0">Không Kiểm Tra</option>
            <option value="1">Giống</option>
        </select>
        <button id="Delete${indexOfDirect}" onclick="DeleteDirect(${indexOfDirect})">Delete Direct</button>
        <br>
        <input type="text" class="" id="PayloadCheck${indexOfDirect}" placeholder="PayloadCheck" value="${KichBan[indexOfDirect].PayloadCheck}""></input>
        <select id="PayloadCheckType${indexOfDirect}">
            <option selected="selected" value="${KichBan[indexOfDirect].PayloadCheckType}" style="color:red;">${PayloadCheckReplace}</option>
            <option value="-1">Không Giống</option>
            <option value="0">Không Kiểm Tra</option>
            <option value="1">Giống</option>
        </select>
        <br>
        <input type="text" class="" id="QuickMessageCheck${indexOfDirect}" value="${KichBan[indexOfDirect].QuickMessageCheck}" placeholder="QuickMessageCheck"></input>
        <select id="QuickCheckType${indexOfDirect}">
            <option selected="selected" value="${KichBan[indexOfDirect].QuickCheckType}" style="color:red;">${QuickCheckTypeReplace}</option>
            <option value="-1">Không Giống</option>
            <option value="0">Không Kiểm Tra</option>
            <option value="1">Giống</option>
        </select>
        <br>
        <button class="btn_4" onclick="CreateActionDetailHTML(${indexOfDirect},)" id="ActionButton${indexOfDirect}">Thêm action</button>
        <div id="Action${indexOfDirect}"></div>
        `;
        return htmlString;    
}
function CreateActionDetailHTML(indexScriptString){
    var indexScript = parseInt(indexScriptString);
    var indexAction = indexActionList[indexScript];
    var div = document.createElement('div');
    div.id = `ActionDetail${indexScript}-${indexAction}`;
    div.innerHTML = `
    <br>
    <select id="Type${indexScript}-${indexAction}" onchange="CheckAndCreateActionOption(${indexScript},${indexAction})">
            <option value="none">none</option>
            <option value="Send">Send</option>
            <option value="Show">Show</option>
            <option value="Update">Update</option>    
    </select>
    <div id= "optionValue${indexScript}-${indexAction}">
    </div>
    `;

    document.getElementById(`Action${indexScriptString}`).appendChild(div);
    indexActionList[indexScript]++;
}
function CreateActionDetailHTMLWithKichBan(indexScriptString,KichBan){
    var indexScript = parseInt(indexScriptString);
    var indexAction = indexActionList[indexScript];
    var div = document.createElement('div');
    div.id = `ActionDetail${indexScript}-${indexAction}`;
    div.innerHTML = `
    <br>
    <select id="Type${indexScript}-${indexAction}" onchange="CheckAndCreateActionOptionWithKichBan(${indexScript},${indexAction},${KichBan})">
                <option value="${KichBan.Type}" style="color:red;">${KichBan.Type}</option>
                <option value="Send">Send</option>
                <option value="Show">Show</option>
                <option value="Update">Update</option>    
    </select>
    <div id= "optionValue${indexScript}-${indexAction}">
    </div>
    `;

    document.getElementById(`Action${indexScriptString}`).appendChild(div);
    indexActionList[indexScript]++;
}

function CheckAndCreateActionOptionWithKichBan(indexScript,indexAction,KichBan){
    var typeOption = KichBan.Type;
    var innerHTML = "";
    switch(typeOption){
        case "Send":
            innerHTML = `
            <select id="Content${indexScript}-${indexAction}" onchange="CheckAndCreateSecondActionOption(${indexScript},${indexAction})">
                <option value="${KichBan.Content}">${KichBan.Content}</option>
                <option value="StructureMess">StructureMess</option>    
                <option value="NormalMess">NormalMess</option>  
                <option value="QuickMess">QuickMess</option> 
            </select>        
            <div id="OptionChosen${indexScript}-${indexAction}"></div>
            `
            break;
        case "Show":
            innerHTML = `
            <select id="Content${indexScript}-${indexAction}">
                <option style="color:red;" value=${KichBan.Content}>${KichBan.Content}</option>
                <option value="FeatureProduct">FeatureProduct</option>
                <option value="SearchProduct">SearchProduct</option>
                <option value="ListNameType">ListNameType</option>
            </select>
            <input type="text" id="SubValue${indexScript}-${indexAction}" value="${KichBan.SubValue}" placeholder="SubValue"></input>  
            `
            break;
        case "Update":
            innerHTML = `
            <select id="Content${indexScript}-${indexAction}">
                <option style="color:red;" value=${KichBan.Content}>${KichBan.Content}</option>
                <option value="Status">Status</option>
                <option value="Temp">Temp</option>
                <option value="Order">Order</option>
                <option value="Phone">Phone</option>
                <option value="Address">Address</option>
            </select>
            <input type="text" id="Value${indexScript}-${indexAction}" value="${KichBan.Value}" placeholder="Value"></input>
            <input type="text" id="SubValue${indexScript}-${indexAction}" value="${KichBan.SubValue}" placeholder="SubValue"></input>  
            `
            break;
        
    }
    document.getElementById(`optionValue${indexScript}-${indexAction}`).innerHTML = innerHTML;
    if(KichBan.Type == "Send")
    {
        CheckAndCreateSecondActionOptionWithKichBan(indexScript,indexAction,KichBan);
    }
}

function CheckAndCreateActionOption(indexScript,indexAction){
    var typeOption = document.getElementById(`Type${indexScript}-${indexAction}`).value;
    var innerHTML = "";
    switch(typeOption){
        case "Send":
            innerHTML = `
            <select id="Content${indexScript}-${indexAction}" onchange="CheckAndCreateSecondActionOption(${indexScript},${indexAction})">
                <option value="none">none</option>
                <option value="StructureMess">StructureMess</option>    
                <option value="NormalMess">NormalMess</option>  
                <option value="QuickMess">QuickMess</option> 
            </select>        
            <div id="OptionChosen${indexScript}-${indexAction}"></div>
            `
            break;
        case "Show":
            innerHTML = `
            <select id="Content${indexScript}-${indexAction}">
                <option value="FeatureProduct">FeatureProduct</option>
                <option value="SearchProduct">SearchProduct</option>
                <option value="ListNameType">ListNameType</option>
            </select>
            <input type="text" id="SubValue${indexScript}-${indexAction}" placeholder="SubValue"></input>  
            `
            break;
        case "Update":
            innerHTML = `
            <select id="Content${indexScript}-${indexAction}">
                <option value="Status">Status</option>
                <option value="Temp">Temp</option>
                <option value="Order">Order</option>
                <option value="Phone">Phone</option>
                <option value="Address">Address</option>
            </select>
            <input type="text" id="Value${indexScript}-${indexAction}" placeholder="Value"></input>
            <input type="text" id="SubValue${indexScript}-${indexAction}" placeholder="SubValue"></input>  
            `
            break;
    }
    document.getElementById(`optionValue${indexScript}-${indexAction}`).innerHTML = innerHTML;
}
function CheckAndCreateSecondActionOption(indexScript,indexAction)
{
    var keyword = `${indexScript}-${indexAction}`;
    var findValue = hashMap.find(x => x.key == keyword);
    if(findValue != null){
        hashMap.pop(findValue);
    }
    var typeContent = document.getElementById(`Content${indexScript}-${indexAction}`).value;
    if(typeContent == "StructureMess")
    {
        var innerHTML=
        `
            <input type="text" id="Value${indexScript}-${indexAction}" placeholder="Value"></input>
            <input type="text" id="SubValue${indexScript}-${indexAction}" placeholder="SubValue"></input>
            <input type="text" id="DescValue${indexScript}-${indexAction}" placeholder="DescValue"></input>
            <input type="text" id="SubType${indexScript}-${indexAction}" placeholder="SubType"></input>
            <button onclick="OptionCreator(${indexScript},${indexAction})" class="btn-dark">Thêm Option</button>
            <div id="QuickMessOption${indexScript}-${indexAction}"></div>
        `
    }
    if(typeContent == "NormalMess")
    {
        var innerHTML=
        `
            <input type="text" id="Value${indexScript}-${indexAction}" placeholder="Value"></input>
        `
    }
    if(typeContent == "QuickMess")
    {
        var innerHTML=
        `
            <input type="text" id="Value${indexScript}-${indexAction}" placeholder="Value"></input>
            <button onclick="OptionCreator(${indexScript},${indexAction})" class="btn-dark">Thêm Option</button>
            <div id="QuickMessOption${indexScript}-${indexAction}"></div>
        `
    }
    document.getElementById(`OptionChosen${indexScript}-${indexAction}`).innerHTML=innerHTML;
}

function CheckAndCreateSecondActionOptionWithKichBan(indexScript,indexAction,KichBan)
{
    var keyword = `${indexScript}-${indexAction}`;
    var findValue = hashMap.find(x => x.key == keyword);
    if(findValue != null){
        hashMap.pop(findValue);
    }
    var typeContent = KichBan.Content;
    if(typeContent == "StructureMess")
    {
        var innerHTML=
        `
            <input type="text" id="Value${indexScript}-${indexAction}" value="${KichBan.Value}" placeholder="Value"></input>
            <input type="text" id="SubValue${indexScript}-${indexAction}" value="${KichBan.SubValue}" placeholder="SubValue"></input>
            <input type="text" id="DescValue${indexScript}-${indexAction}" value="${KichBan.DescValue}" placeholder="DescValue"></input>
            <input type="text" id="SubType${indexScript}-${indexAction}" value="${KichBan.SubType}" placeholder="SubType"></input>
            <button onclick="OptionCreator(${indexScript},${indexAction})" class="btn-dark">Thêm Option</button>
            <div id="QuickMessOption${indexScript}-${indexAction}"></div>
        `
    }
    if(typeContent == "NormalMess")
    {
        var innerHTML=
        `
            <input type="text" id="Value${indexScript}-${indexAction}" value="${KichBan.Value}" placeholder="Value"></input>
        `
    }
    if(typeContent == "QuickMess")
    {
        var innerHTML=
        `
            <input type="text" id="Value${indexScript}-${indexAction}" value="${KichBan.Value}" placeholder="Value"></input>
            <button onclick="OptionCreator(${indexScript},${indexAction})" class="btn-dark">Thêm Option</button>
            <div id="QuickMessOption${indexScript}-${indexAction}"></div>
        `
    }
    document.getElementById(`OptionChosen${indexScript}-${indexAction}`).innerHTML=innerHTML;
}
function OptionCreator(indexScript,indexAction){
    var keyword = `${indexScript}-${indexAction}`;
    var findValue = hashMap.find(x => x.key == keyword);
    if(findValue == null){
        findValue = {
            key : `${indexScript}-${indexAction}`,
            value : 0
            };
        hashMap.push(findValue);
    }
    var div1 = document.createElement('div')
    div1.id = `QuickMessOptionDetail${indexScript}-${indexAction}`;
    div1.innerHTML = `
        <input id="Text${indexScript}-${indexAction}-${findValue.value}" placeholder="Text"></input>
        <input id="Payload${indexScript}-${indexAction}-${findValue.value}" placeholder="Payload"></input>
        <input id="ButtonType${indexScript}-${indexAction}-${findValue.value}" placeholder="ButtonType"></input>
        <br>    
    `
    findValue.value++;
    document.getElementById(`QuickMessOption${indexScript}-${indexAction}`).appendChild(div1);
}
function OptionCreatorWithKichBan(indexScript,indexAction,KichBan){
    var keyword = `${indexScript}-${indexAction}`;
    var findValue = hashMap.find(x => x.key == keyword);
    if(findValue == null){
        findValue = {
            key : `${indexScript}-${indexAction}`,
            value : 0
            };
        hashMap.push(findValue);
    }
    var div1 = document.createElement('div')
    div1.id = `QuickMessOptionDetail${indexScript}-${indexAction}`;
    div1.innerHTML = `
        <input id="Text${indexScript}-${indexAction}-${findValue.value}" value="${KichBan.Text}" placeholder="Text"></input>
        <input id="Payload${indexScript}-${indexAction}-${findValue.value}" value="${KichBan.Payload}" placeholder="Payload"></input>
        <input id="ButtonType${indexScript}-${indexAction}-${findValue.value}" value="${KichBan.ButtonType}" placeholder="ButtonType"></input>
        <br>    
    `
    findValue.value++;
    document.getElementById(`QuickMessOption${indexScript}-${indexAction}`).appendChild(div1);
}
function Submit(){
    var listScript = [];
    for(let i = 0; i < indexScript; i++) {
        if(DeletedDirect.find(x => x == i) == null)
        {
            var script = CreateScriptData(i);
            listScript.push(script);
        }
    }
    var StatusId = EditingID;
    if(StatusId == "" || StatusId == null)
    {
        alert("Chọn IdStatus");
    }
    else
    {
        SubmitHander(listScript,DeletedDirect);
    }
}
function CreateScriptData(index){
    var script = {
        TextCheck: "",
        TextCheckType: 0,
        PayloadCheck: "",
        PayloadCheckType: -1,
        QuickMessageCheck: "",
        QuickCheckType: 0,
        Action: []
    }
    script.TextCheck = document.getElementById(`TextCheck${index}`).value;
    script.TextCheckType = document.getElementById(`TextCheckType${index}`).value;
    script.PayloadCheck = document.getElementById(`PayloadCheck${index}`).value;
    script.PayloadCheckType = document.getElementById(`PayloadCheckType${index}`).value;
    script.QuickMessageCheck = document.getElementById(`QuickMessageCheck${index}`).value;
    script.QuickCheckType = document.getElementById(`QuickCheckType${index}`).value;
    for (let i = 0; i < indexActionList[index]; i++) {
        var action = CreateAndGetActionData(index,i);
        script.Action.push(action);
    }
    return script;
}

function CreateAndGetActionData(indexScriptAction,indexAction){
    var action = {
        Type: "",
        Content: "",
        Value: "",
        SubValue: "",
        DescValue: "",
        SubType: "",
        Option: []
    }
    action.Type = document.getElementById(`Type${indexScriptAction}-${indexAction}`).value;
    action.Content = document.getElementById(`Content${indexScriptAction}-${indexAction}`).value;
    var actionValue =  document.getElementById(`Value${indexScriptAction}-${indexAction}`);
    if(actionValue!= null){
        action.Value = actionValue.value
    }
    actionValue =  document.getElementById(`SubValue${indexScriptAction}-${indexAction}`);
    if(actionValue!= null){
        action.SubValue = actionValue.value
    }
    actionValue =  document.getElementById(`DescValue${indexScriptAction}-${indexAction}`);
    if(actionValue!= null){
        action.DescValue = actionValue.value
    }
    actionValue =  document.getElementById(`SubType${indexScriptAction}-${indexAction}`);
    if(actionValue!= null){
        action.SubType = actionValue.value
    }
    
    var key = `${indexScriptAction}-${indexAction}`;
    var findValue = hashMap.find(x => x.key == key);
    if(findValue != null){
        for (let i = 0; i < findValue.value; i++) {
            var button = CreateAndGetButtonData(indexScriptAction,indexAction,i);
            action.Option.push(button);
        }
    }
    
    return action;
}
function CreateAndGetButtonData(indexScriptAction, indexAction,indexButton){
    var button = {
        Text: "Mua hàng",
        Payload: "BUY",
        ButtonType: "Payload"
    }
    button.Text = document.getElementById(`Text${indexScriptAction}-${indexAction}-${indexButton}`).value;
    button.Payload = document.getElementById(`Payload${indexScriptAction}-${indexAction}-${indexButton}`).value;
    button.ButtonType = document.getElementById(`ButtonType${indexScriptAction}-${indexAction}-${indexButton}`).value;
    return button;
}
function SubmitHander(listScript)
{
    var ObjectScript = listScript;
    console.log(ObjectScript);
    var url = "https://e9ba-103-245-246-232.ngrok.io/updatescript";
    var menuId = $("ul.nav").first().attr("id");
    var StatusId = EditingID;
    var request = $.ajax({
        url: url,
        type: "POST",
        data: {
            pageid: pageId,
            itemid: StatusId, 
            item: ObjectScript
        },
        dataType: "html"
        });

        request.done(function(msg) {
        $("#log").html( msg );
        });

        request.fail(function(jqXHR, textStatus) {
        alert( "Request failed: " + textStatus );
        });
}