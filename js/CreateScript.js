let indexScript = 0;
let indexActionList = [];
let hashMap = [];
function CreateMoreDirect(){
    var scriptHTML = CreateConditionHTML(indexScript);
    indexScript+=1;
    indexActionList.push(0);
    var MainDiv = document.createElement('div');
    MainDiv.id = `MainDiv${indexScript}`;
    MainDiv.innerHTML = scriptHTML;
    document.getElementById("ScriptHTML").appendChild(MainDiv);
    indexOfAction=0;
}
function CreateConditionHTML(indexOfDirect){
    var htmlString = `
        <input type="text" id="TextCheck${indexOfDirect}" placeholder="TextCheck"></input>
        <input type="text" id="TextCheckType${indexOfDirect}" placeholder="TextCheckType"></input>
        <br>
        <input type="text" id="PayloadCheck${indexOfDirect}" placeholder="PayloadCheck"></input>
        <input type="text" id="PayloadCheckType${indexOfDirect}" placeholder="PayloadCheckType"></input>
        <br>
        <input type="text" id="QuickMessageCheck${indexOfDirect}" placeholder="QuickMessageCheck"></input>
        <input type="text" id="QuickCheckType${indexOfDirect}" placeholder="QuickCheckType"></input>
        <br>
        <button onclick="CreateActionDetailHTML(${indexOfDirect})">Thêm action</button>
        <div id="Action${indexOfDirect}"></div>
        <br>
        `;
    return htmlString;
}
function CreateActionDetailHTML(indexScriptString){
    var indexScript = parseInt(indexScriptString);
    var indexAction = indexActionList[indexScript];
    var div = document.createElement('div');
    div.id = `ActionDetail${indexScript}-${indexAction}`;
    div.innerHTML = `
    <select id="Type${indexScript}-${indexAction}" onchange="CheckAndCreateActionOption(${indexScript},${indexAction})">
                <option value="none">none</option>
                <option value="Send">Send</option>
                <option value="Show">Show</option>
                <option value="Update">Update</option>    
    </select>
    <div id= "optionValue${indexScript}-${indexAction}">
    </div>
    `;

    document.getElementById(`Action${indexScriptString}`).appendChild(div);
    indexActionList[indexScript]++;
}
function CheckAndCreateActionOption(indexScript,indexAction){
    var typeOption = document.getElementById(`Type${indexScript}-${indexAction}`).value;
    var innerHTML = "";
    switch(typeOption){
        case "Send":
            innerHTML = `
            <select id="Content${indexScript}-${indexAction}" onchange="CheckAndCreateSecondActionOption(${indexScript},${indexAction})">
                <option value="none">none</option>
                <option value="structuremess">structuremess</option>    
                <option value="NormalMess">NormalMess</option>  
                <option value="QuickMess">QuickMess</option> 
            </select>        
            <div id="OptionChosen${indexScript}-${indexAction}"></div>
            `
            break;
        case "Show":
            innerHTML = `
            <select id="Content${indexScript}-${indexAction}">
                <option value="FeatureProduct">FeatureProduct</option>
                <option value="SearchProduct">SearchProduct</option>
                <option value="ListNameType">ListNameType</option>
            </select>
            <input type="text" id="SubValue${indexScript}-${indexAction}" placeholder="SubValue"></input>  
            `
            break;
        case "Update":
            innerHTML = `
            <select id="Content${indexScript}-${indexAction}">
                <option value="Status">Status</option>
                <option value="Temp">Temp</option>
                <option value="Order">Order</option>
                <option value="Phone">Phone</option>
                <option value="Address">Address</option>
            </select>
            <input type="text" id="Value${indexScript}-${indexAction}" placeholder="Value"></input>
            <input type="text" id="SubValue${indexScript}-${indexAction}" placeholder="SubValue"></input>  
            `
            break;
    }
    document.getElementById(`optionValue${indexScript}-${indexAction}`).innerHTML = innerHTML;
}
function CheckAndCreateSecondActionOption(indexScript,indexAction)
{
    var keyword = `${indexScript}-${indexAction}`;
    var findValue = hashMap.find(x => x.key == keyword);
    console.log(findValue);
    console.log("Call");
    if(findValue != null){
        hashMap.pop(findValue);
        console.log("Call");
    }
    console.log(hashMap);
    var typeContent = document.getElementById(`Content${indexScript}-${indexAction}`).value;
    if(typeContent == "structuremess")
    {
        var innerHTML=
        `
            <input type="text" id="Value${indexScript}-${indexAction}" placeholder="Value"></input>
            <input type="text" id="SubValue${indexScript}-${indexAction}" placeholder="SubValue"></input>
            <input type="text" id="DescValue${indexScript}-${indexAction}" placeholder="DescValue"></input>
            <input type="text" id="SubType${indexScript}-${indexAction}" placeholder="SubType"></input>
            <button onclick="OptionCreator(${indexScript},${indexAction})">Thêm Option</button>
            <div id="QuickMessOption${indexScript}-${indexAction}"></div>
        `
    }
    if(typeContent == "NormalMess")
    {
        var innerHTML=
        `
            <input type="text" id="Value${indexScript}-${indexAction}" placeholder="Value"></input>
        `
    }
    if(typeContent == "QuickMess")
    {
        var innerHTML=
        `
            <input type="text" id="Value${indexScript}-${indexAction}" placeholder="Value"></input>
            <button onclick="OptionCreator(${indexScript},${indexAction})">Thêm Option</button>
            <div id="QuickMessOption${indexScript}-${indexAction}"></div>
        `
    }
    document.getElementById(`OptionChosen${indexScript}-${indexAction}`).innerHTML=innerHTML;
}
function OptionCreator(indexScript,indexAction){
    var keyword = `${indexScript}-${indexAction}`;
    var findValue = hashMap.find(x => x.key == keyword);
    if(findValue == null){
        findValue = {
            key : `${indexScript}-${indexAction}`,
            value : 0
            };
        hashMap.push(findValue);
    }
    var div1 = document.createElement('div')
    div1.id = `QuickMessOptionDetail${indexScript}-${indexAction}`;
    div1.innerHTML = `
        <input id="Text${indexScript}-${indexAction}-${findValue.value}" placeholder="Text"></input>
        <input id="Payload${indexScript}-${indexAction}-${findValue.value}" placeholder="Payload"></input>
        <input id="ButtonType${indexScript}-${indexAction}-${findValue.value}" placeholder="ButtonType"></input>
        <br>    
    `
    findValue.value++;
    document.getElementById(`QuickMessOption${indexScript}-${indexAction}`).appendChild(div1);
}
function CreateNewScriptDetail(){
    var script = {
    TextCheck: "",
    TextCheckType: 0,
    PayloadCheck: "",
    PayloadCheckType: 0,
    QuickMessageCheck: "",
    QuickCheckType: 0,
    Action: []
    };
    return script;
}
function Submit(){
    var listScript = [];
    for (let i = 0; i < indexScript; i++) {
        var script = CreateScriptData(i);
        listScript.push(script);
    }
    console.log(listScript);
    SubmitHander(listScript);
}
function CreateScriptData(index){
    var script = {
        TextCheck: "",
        TextCheckType: 0,
        PayloadCheck: "",
        PayloadCheckType: -1,
        QuickMessageCheck: "",
        QuickCheckType: 0,
        Action: []
    }
    script.TextCheck = document.getElementById(`TextCheck${index}`).value;
    script.TextCheckType = document.getElementById(`TextCheckType${index}`).value;
    script.PayloadCheck = document.getElementById(`PayloadCheck${index}`).value;
    script.PayloadCheckType = document.getElementById(`PayloadCheckType${index}`).value;
    script.QuickMessageCheck = document.getElementById(`QuickMessageCheck${index}`).value;
    script.QuickCheckType = document.getElementById(`QuickCheckType${index}`).value;
    for (let i = 0; i < indexActionList[index]; i++) {
        var action = CreateAndGetActionData(index,i);
        script.Action.push(action);
    }
    return script;
}

function CreateAndGetActionData(indexScriptAction,indexAction){
    var action = {
        Type: "",
        Content: "",
        Value: "",
        SubValue: "",
        DescValue: "",
        SubType: "",
        Option: []
    }
    action.Type = document.getElementById(`Type${indexScriptAction}-${indexAction}`).value;
    action.Content = document.getElementById(`Content${indexScriptAction}-${indexAction}`).value;
    var actionValue =  document.getElementById(`Value${indexScriptAction}-${indexAction}`);
    if(actionValue!= null){
        action.Value = actionValue.value
    }
    actionValue =  document.getElementById(`SubValue${indexScriptAction}-${indexAction}`);
    if(actionValue!= null){
        action.SubValue = actionValue.value
    }
    actionValue =  document.getElementById(`DescValue${indexScriptAction}-${indexAction}`);
    if(actionValue!= null){
        action.DescValue = actionValue.value
    }
    actionValue =  document.getElementById(`SubType${indexScriptAction}-${indexAction}`);
    if(actionValue!= null){
        action.SubType = actionValue.value
    }
    
    var key = `${indexScriptAction}-${indexAction}`;
    var findValue = hashMap.find(x => x.key == key);
    if(findValue != null){
        for (let i = 0; i < findValue.value; i++) {
            var button = CreateAndGetButtonData(indexScriptAction,indexAction,i);
            action.Option.push(button);
        }
    }
    
    return action;
}
function CreateAndGetButtonData(indexScriptAction, indexAction,indexButton){
    var button = {
        Text: "Mua hàng",
        Payload: "BUY",
        ButtonType: "Payload"
    }
    button.Text = document.getElementById(`Text${indexScriptAction}-${indexAction}-${indexButton}`).value;
    button.Payload = document.getElementById(`Payload${indexScriptAction}-${indexAction}-${indexButton}`).value;
    button.ButtonType = document.getElementById(`ButtonType${indexScriptAction}-${indexAction}-${indexButton}`).value;
    return button;
}
function SubmitHander(listScript)
{
    var ObjectScript = listScript;
    var url = "https://6e76-115-79-218-170.ngrok.io/UpdateOrder";
    var menuId = $("ul.nav").first().attr("id");
        
    var request = $.ajax({
        url: url,
        type: "POST",
        data: {id : ObjectScript},
        dataType: "html"
        });

        request.done(function(msg) {
        $("#log").html( msg );
        });

        request.fail(function(jqXHR, textStatus) {
        alert( "Request failed: " + textStatus );
        });
}