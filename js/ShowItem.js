var pageIDWholePageForShowItem;
function FiveItemEach(pageIDWholePage)
{
    pageIDWholePageForShowItem = pageIDWholePage;
    var pageId = pageIDWholePageForShowItem;
    var url = "https://condieuchatbot.xyz/getlistproductid";
    var menuId = $("ul.nav").first().attr("id");
    var request = $.ajax({
        url: url,
        type: "POST",
        data: { 
            pageid: pageId
        },
        dataType: "html",
        success: function(data) {
            var DataIncomeObj = JSON.parse(data);
            console.log("------------");
            console.log(pageIDWholePageForShowItem);
            console.log("------------");
            Pagination(DataIncomeObj.item)
        },
        error: function(request, error) {
            alert("Request: " + JSON.stringify(request));
        }
    });

    request.done(function(msg) {
        $("#log").html(msg);
    });

    request.fail(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}
var DataIncomeObject;
function Pagination(DataIncomeObj)
{
    var TotalPage = Math.ceil(DataIncomeObj.length / 5);
    console.log(TotalPage);
    DataIncomeObject = DataIncomeObj;
    ShowInPage(1);
    var innerHTML = 
    `
        <button style="margin" class="btn" style="margin: auto;" style="width: 50%;" style="padding: 10px;" style="color:red;" onclick=ShowInPage(1)>Đầu tiên</button>
    `
    document.getElementById("Pagination").innerHTML+=innerHTML;
    for(var i = 1; i <= TotalPage; i++)
    {
        innerHTML = 
        `
            <button style="margin: auto;" style="width: 50%;" style="padding: 10px;" class="btn" style="color:red;" onclick=ShowInPage(${i})>${i}</button>
        `
        document.getElementById("Pagination").innerHTML+=innerHTML;
    }
    innerHTML = 
    `
        <button style="margin: auto;" style="width: 50%;" style="padding: 10px;" class="btn" style="color:red;" onclick=ShowInPage(${TotalPage})>Cuối cùng</button>
    `
    document.getElementById("Pagination").innerHTML+=innerHTML;
    
}

function ShowInPage(i)
{
    var productHTML = "";
    document.getElementById("content-shower").innerHTML = productHTML;
    var indexPaging = (i - 1) * 5;
    console.log(indexPaging);
    for(var i = 0; i < 5; i++)
    {
        SubmitHanderGetData(DataIncomeObject[indexPaging]);
        indexPaging++;
    }
}

function SubmitHanderGetData(IdScript) {
    var pageId = pageIDWholePageForShowItem;
    var scriptID = IdScript;
    var url = "https://condieuchatbot.xyz/getproduct";
    var menuId = $("ul.nav").first().attr("id");
    var request = $.ajax({
        url: url,
        type: "POST",
        data: { 
            pageid: pageId,
            itemid: scriptID
        },
        dataType: "html",
        success: function(data) {
            var DataIncomeObj = JSON.parse(data);
            ShowAll(DataIncomeObj.item,IdScript);
        },
        error: function(request, error) {
            alert("Request: " + JSON.stringify(request));
        }
    });

    request.done(function(msg) {
        $("#log").html(msg);
    });

    request.fail(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}
function ShowAll(DataIncomeObj,Index)
{
    ProdutHTML(DataIncomeObj,Index);
    MiniPrice(DataIncomeObj,Index);
}
function MiniPrice(DataIncomeObj,i)
{
    if(DataIncomeObj.Price != null)
    {
        for (let j = 0; j < DataIncomeObj.Price.length; j++)
        {
            var mininametypeHTML = `<li>${DataIncomeObj.Price[j].NameType}</li>`
            var minipriceHTML = `<li>${DataIncomeObj.Price[j].Price}</li> <br>`;
            document.getElementById(`${i}MiniPrice`).innerHTML+=mininametypeHTML;
            document.getElementById(`${i}MiniPrice`).innerHTML+=minipriceHTML;
        }
    }
}
function ProdutHTML(DataIncomeObj,i)
{
    var productHTML = `
        <div class="tab-pane fade show active" id="nav-home${i}-${DataIncomeObj.ID}" role="tabpanel" aria-labelledby="nav-home-tab">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="single-product mb-60">
                        <div class="product-img">
                            <img src="${DataIncomeObj.Image}" alt="">
                        <div class="new-product">
                            <span>New</span>
                        </div>
                    </div>
                <div class="product-caption">
                    <h4><a href="#">${DataIncomeObj.Name}</a></h4>
                    <div class="price">
                        <ul>
                            <div id="${i}MiniPrice"></div>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-8 col-lg-8 col-md-6">
            <p>${DataIncomeObj.Description}</p>
            <div class="row">
            <form action="suaitem.php" method="GET" class="p-4">
                <input type="hidden" name="IdFix" value="${DataIncomeObj.ID}">
                <input type="hidden" name="PageId" value="${pageIDWholePageForShowItem}">
                <input type="submit" id="FixItemInfo${i}" class="btn btn-primary" role="button" value="Sửa thông tin">
            </form>
            <form onclick="SendDelete(${DataIncomeObj.ID})" class="p-4">
                <input type="hidden" name="masp" value="SP_10">
                <input type="submit" id="DeleteItem${i}" class="btn btn-primary" role="button" value="Xoá sản phẩm">
            </form>
            </div>
        </div>
        </div><div class="row">
    </div>
    `

document.getElementById("content-shower").innerHTML+=productHTML;
}
function SendDelete(DeleteDataIncomeObjID)
{
    console.log(DeleteDataIncomeObjID);
    var pageId = pageIDWholePageForShowItem;
    var scriptID = DeleteDataIncomeObjID;
    var url = "https://condieuchatbot.xyz/deleteproduct";
    var menuId = $("ul.nav").first().attr("id");
    var request = $.ajax({
        url: url,
        type: "POST",
        data: { 
            pageid: pageId,
            itemid: scriptID
        },
        dataType: "html",
        success: function(data) {
            var DataIncomeObj = JSON.parse(data);
        },
        error: function(request, error) {
            alert("Request: " + JSON.stringify(request));
        }
    });

    request.done(function(msg) {
        $("#log").html(msg);
    });

    request.fail(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}