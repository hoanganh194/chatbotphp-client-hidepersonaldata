function AjaxReceiver(pageID,urlReceiver)
{
    pageId = pageID;
    var url = urlReceiver;
    var menuId = $("ul.nav").first().attr("id");
    var request = $.ajax({
        url: url,
        type: "POST",
        data: { 
            pageid: pageId
        },
        dataType: "html",
        success: function(data) {
            alert("Success");
            var JsonData = JSON.parse(data);
            alert(JsonData);
            return JsonData;
        },
        error: function(request, error) {
            alert("Request: " + JSON.stringify(request));
        }
    });

    request.done(function(msg) {
        $("#log").html(msg);
    });

    request.fail(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}