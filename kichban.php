<?php
require_once("login-handle.php");
?>
<!DOCTYPE html>
<html html class="no-js" lang="zxx">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Trang Nhân Viên Công Ty Sữa Hữu Cơ</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="/img/favicon.ico">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- CSS here -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/flaticon.css">
        <link rel="stylesheet" href="css/slicknav.css">
        <link rel="stylesheet" href="css/animate.min.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/fontawesome-all.min.css">
        <link rel="stylesheet" href="css/themify-icons.css">
        <link rel="stylesheet" href="css/slick.css">
        <link rel="stylesheet" href="css/nice-select.css">
        <link rel="stylesheet" href="css/style.css">
        <script>
</script>
    </head>
    <header>
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header ">
                <div class="header-top top-bg d-none d-lg-block">
                    <div class="container-fluid">
                        <div class="col-xl-12">
                            <div class="row d-flex justify-content-between align-items-center">
                                <div class="header-info-left d-flex">

                                    <!-- <div class="select-this">
                                        <form action="#">
                                            <div class="select-itms">
                                                <select name="sxelect" id="select1">
                                                    <option value="">Vietnam</option>
                                                </select>
                                            </div>
                                        </form>
                                    </div> -->
                                    <ul class="contact-now">
                                        <a href="index.php">
                                            <li>Công ty sữa hữu cơ Việt Nam</li>
                                        </a>
                                    </ul>
                                </div>
                                <div class="header-info-right">
                                    <ul>
                                        <li> 
                                            <img src="/NhanVien/NV_2.png" alt="" style="width: 10%">
                                            <a href="index.php" id="UserName"></a>
                                        </li>
            <li><a href="/XLNhapThongTin/ThongTin"> Quản lý tài khoản</a></li>
            <li><a href="/XLLogOut">logout</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-bottom  header-sticky">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <!-- Logo -->
                            <div class="col-xl-1 col-lg-1 col-md-1 col-sm-3">

                            </div>
                            <div class="col-xl-6 col-lg-8 col-md-7 col-sm-5">
                                <!-- Main-menu -->
                                <div class="main-menu f-left d-none d-lg-block">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a href="index.php">Home</a></li>
                                            <li><a href="additem.php">Thêm sản phẩm mới</a></li>
                                            <li><a href="danhsachhoadon.php">Danh sách hoá đơn</a></li>
                                            <li><a href="kichban.php">Tạo kịch bản</a><li>
                                            <li><a href="showkichban.php">Xem Kịch Bản</a><li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-3 col-md-3 col-sm-3 fix-card">
                                <ul class="header-right f-right d-none d-lg-block d-flex justify-content-between">
                                    <li class="d-none d-xl-block">
                                        <div class="form-box f-right ">
                                            <form action="/TimKiem" method="POST">
                    <input type="text" name="search" placeholder="Search products" value="">
                </form>
                                        </div>
                                    </li>
                                    
                                </ul>
                            </div>
                            <!-- Mobile Menu -->
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>
<main>
    <body onload="wtf()">
        <div class="col-md-12 form-group p_star">
            <button class="btn_3" onclick="CreateMoreDirect(pageIDWholePage)">Tạo Thêm Direct</button>
            <input type="text" style="margin-top:2rem" id="IDStatus" placeholder="ID Status"></input>
        </div>
        <div id=ScriptHTML></div>
        <div class="col-md-12 form-group"></div>
            <button class="btn_3" style="margin-left: 30rem;" onclick="Submit()">Submit</button>
        </div>
    </body>
</main>
    <script>
        var pageIDWholePage;
        function wtf()
        {
            var pageID = <?php echo($_SESSION['pageID']);?>;
            pageIDWholePage = pageID;
            var name = "<?php echo($_SESSION['username']);?>";
            document.getElementById(`UserName`).innerHTML="Xin chào: ";
            document.getElementById(`UserName`).innerHTML+=name;
        }
    </script>



    <script src="js/ScriptCreator.js"></script>
    <!-- All JS Custom Plugins Link Here here -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/slick.min.js"></script>

    <!-- One Page, Animated-HeadLin -->
    <script src="js/wow.min.js"></script>
    <script src="js/animated.headline.js"></script>
    <script src="js/jquery.magnific-popup.js"></script>

    <!-- Scrollup, nice-select, sticky -->
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.sticky.js"></script>

    <!-- contact js -->
    <script src="js/contact.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/mail-script.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>

    <!-- Jquery Plugins, main Jquery -->
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>

