<?php
    function CreateNewUserDataEntity(){
        return  $UserData = array(
            "ID" => -1,
            "Address"=> "",
            "Phone"=> "",
            "Email"=> "",
            "Status"=> 1,
            "Temp"=> "",
            "Order"=>[],
            "Command"=>0
        );
    }
    function CreateNewProductDataEntity(){
        return  $product = array(
            "ID" => -1,
            "Name"=>"",
            "Price"=> [],
            "Image"=> "",
            "Description"=>""
        );
    }
    function CreateNewOrderDataEntity(){
        return  $order = array(
            "ID" => -1,
            "UserID"=>"",
            "Name"=>"",
            "Phone"=> "",
            "Address"=> "",
            "Order"=> [],
            "OrderDay"=>"",
            "Confirm"=>0,
            "Deliveried"=>0
        );
    }
    function CreateNewOrderLineEntity(){
        return $orderLine = array(
            "ID" => -1,
            "ProductID"=>"",
            "ProductName"=>"",
            "PriceLine"=> CreateNewPriceLine()
            );
    }
    function CreateNewPriceLine(){
        return $priceLine = array(
            "ID" => 1,
            "NameType" =>"",
            "Price" =>0,
            "Quantity"=>0
        );
    }
    function CreateNewCommandList(){
        return array(
            "ListAction" =>[]
        );
    }
    function CreateNewCommandLine(){
        return array(
            "TextCheck" => "",
            "TextCheckType" =>0,
            "PayloadCheck" => "",
            "PayloadCheckType" =>0,
            "QuickCheck" => "",
            "QuickCheckType" =>0,
            "Action" =>[]
        );
    }
    function CreateNewActionLine(){
        return array(
            "TypeAction" => "",
            "Content" =>"",
            "Value"=>"",
            "SubValue" =>"",
            "Option" =>[],
            "Action" =>[],
            "ActionElse"=>[]
        );
    }
?>