<?php
session_start();
require_once( 'Facebook/autoload.php' );
if (file_exists(__DIR__ . '/config.php')) {
  $config = include __DIR__ . '/config.php';
}

$fb = new Facebook\Facebook([
  'app_id' => $config['app_id'],
  'app_secret' =>  $config['app_secret'],
  'default_graph_version' => 'v2.9',
  ]);

  $helper = $fb->getRedirectLoginHelper();
 
  if (isset($_GET['state'])) {
      $helper->getPersistentDataHandler()->set('state', $_GET['state']);
  }
   
  try {
    $accessToken = $helper->getAccessToken();
  } catch(Facebook\Exceptions\FacebookResponseException $e) {
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
  } catch(Facebook\Exceptions\FacebookSDKException $e) {
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
  }
   
  if (! isset($accessToken)) {
    if ($helper->getError()) {
      header('HTTP/1.0 401 Unauthorized');
      echo "Error: " . $helper->getError() . "\n";
      echo "Error Code: " . $helper->getErrorCode() . "\n";
      echo "Error Reason: " . $helper->getErrorReason() . "\n";
      echo "Error Description: " . $helper->getErrorDescription() . "\n";
    } else {
      header('HTTP/1.0 400 Bad Request');
      echo 'Bad request';
    }
    exit;
  }
   
  echo '<h3>Access Token</h3>';
  var_dump($accessToken->getValue());
  $accessToken->getValue();
  $response = $fb->get('/me?fields=id,name,email', $accessToken->getValue());
  $oAuth2Client = $fb->getOAuth2Client();
  $tokenMetadata = $oAuth2Client->debugToken($accessToken);
  // echo '<h3>Metadata</h3>';
  // var_dump($tokenMetadata);
   
  $tokenMetadata->validateAppId($config['app_id']);
  $tokenMetadata->validateExpiration();

  var_dump($tokenMetadata);
  if (! $accessToken->isLongLived()) {
    try {
      $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
    } catch (Facebook\Exceptions\FacebookSDKException $e) {
      echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
      exit;
    } 
    // echo '<h3>Long-lived</h3>';
    // var_dump($accessToken->getValue());
  }
  $me = $response->getGraphUser();
  // echo '<br>Logged in as: ' . $me->getName();
  // echo '<br>ID:' . $me->getId();
  // echo '<br>Email:' . $me->getEmail();
  // echo '<br>Token'. $accessToken;

  $pageData = $tokenMetadata->getField('granular_scopes');
  require_once 'db/data.php';
  $dataHandle = new SQLHandle();
  $result = $dataHandle->LoginPageAccount($me->getId());
  if($result){
    $_SESSION['username'] = $me->getName();
    $_SESSION['pageID'] = $result['PAGEID'];
  }
  else{
    $dataHandle->CreateNewPageAccount($me->getId(), $accessToken->getValue(), $pageData[0]['target_ids'][0],  $me->getEmail());
    $_SESSION['username'] = $me->getName();
    $_SESSION['pageID'] = $pageData[0]['target_ids'][0];
  }
  $token = $fb->get($me->getId().'/accounts?access_token='.$accessToken->getValue(),$accessToken->getValue());
  var_dump($token->getDecodedBody()['data'][0]['access_token']);
  var_dump($token->getDecodedBody()['data'][0]['id']);

  // $url = 'https://condieuchatbot.xyz/updatetoken';
  // $data = array('pageid' => $token->getDecodedBody()['data'][0]['id'],
  //  'item' => $token->getDecodedBody()['data'][0]['access_token']);

  // $options = array(
  //     'http' => array(
  //         'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
  //         'method'  => 'POST',
  //         'content' => http_build_query($data)
  //     )
  // );
  // $context  = stream_context_create($options);
  // $result = file_get_contents($url, false, $context);
  // if ($result === FALSE) { var_dump("false"); }
  
  // var_dump($result);
  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL,"https://condieuchatbot.xyz/updatetoken");
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, 
           http_build_query(
             array(
               'pageid' => $token->getDecodedBody()['data'][0]['id'],
               'item' => $token->getDecodedBody()['data'][0]['access_token'],
          )));
  
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  
  $server_output = curl_exec($ch);
  var_dump($server_output);
  curl_close ($ch);
  // if ($server_output == "OK") { ... } else { ... }

  echo '<a href="' . $config['url'] . '">Back to main page</a>';


?>