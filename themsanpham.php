
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Trang Nhân Viên Công Ty Sữa Hữu Cơ</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="/img/favicon.ico">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- CSS here -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/slicknav.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header>
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header ">
                <div class="header-top top-bg d-none d-lg-block">
                    <div class="container-fluid">
                        <div class="col-xl-12">
                            <div class="row d-flex justify-content-between align-items-center">
                                <div class="header-info-left d-flex">

                                    <!-- <div class="select-this">
                                        <form action="#">
                                            <div class="select-itms">
                                                <select name="sxelect" id="select1">
                                                    <option value="">Vietnam</option>
                                                </select>
                                            </div>
                                        </form>
                                    </div> -->
                                    <ul class="contact-now">
                                        <a href="/">
                                            <li>Công ty sữa hữu cơ Việt Nam</li>
                                        </a>
                                    </ul>
                                </div>
                                <div class="header-info-right">
                                    <ul>
                                        <li> <img src="/NhanVien/NV_2.png" alt="" style="width: 10%"><a href="/"> Xin chào: Tô thị bé Bé </a></li>
            <li><a href="/XLNhapThongTin/ThongTin"> Quản lý tài khoản</a></li>
            <li><a href="/XLLogOut">logout</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-bottom  header-sticky">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <!-- Logo -->
                            <div class="col-xl-1 col-lg-1 col-md-1 col-sm-3">

                            </div>
                            <div class="col-xl-6 col-lg-8 col-md-7 col-sm-5">
                                <!-- Main-menu -->
                                <div class="main-menu f-left d-none d-lg-block">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a href="/">Home</a></li>
            <li><a href="/ThemSanPham/ThongTin">Thêm sản phẩm mới</a></li>
            <li><a href="/DanhSachHoaDon">Danh sách hoá đơn</a></li>
            
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-3 col-md-3 col-sm-3 fix-card">
                                <ul class="header-right f-right d-none d-lg-block d-flex justify-content-between">
                                    <li class="d-none d-xl-block">
                                        <div class="form-box f-right ">
                                            <form action="/TimKiem" method="POST">
                    <input type="text" name="search" placeholder="Search products" value="">
                </form>
                                        </div>
                                    </li>
                                    
                                </ul>
                            </div>
                            <!-- Mobile Menu -->
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>

    <main>
         <div class="container">
    <div class="col-lg-6 col-md-6">
        <div class="login_part_form">
            <div class="login_part_form_iner">
                <h3><br> Xin hãy nhập thông tin sản phẩm mới:</h3>
                <!-- <form class="row contact_form" action="/ThemSanPham" method="POST" enctype="multipart/form-data"> -->
                    <div class="col-md-12 form-group p_star">
                        <label>Nhập tên sản phẩm:</label>
                        <input type="text" class="form-control" id="ItemName" name="name" value="" placeholder="Tên sản phẩm" required>
                    </div>
                    <div class="col-md-12 form-group p_star">
                        <label>Nhập đơn giá bán: (Nếu Thêm Type thì sẽ ko nhận Đơn giá bán ở đây)</label>
                        <input type="number" class="form-control" id="ItemDefaultPrice" name="giaban" value="" placeholder="Giá bán" min="0">
                    </div>
                    <div class="col-md-12 form-group p_star">
                        <button onclick="Addtype()">Add Type</button>
                        <div id="AddtypeHTML"></div>
                    </div>
                    <div class="col-md-12 form-group p_star">
                        <label>Nhập thông tin sản phẩm:</label>
                        <textarea class="form-control" id="ItemInfo" name="thongtin" cols="30" rows="10"></textarea>
                    </div>
                    <div class="col-md-12 form-group p_star">
                        <label>Chọn hình ảnh (5mb tối đa, file png):</label>
                        <input type="text" class="form-control" id="ItemImage" name="hinhanh" value="">
                    </div>
                    <div class="col-md-12 form-group">
                        <button onclick="SubmitProduct()" class="btn_3">
                        Xác nhận
                    </button>
                    </div>
                <!-- </form> -->
            </div>
        </div>
    </div>
</div>

    </main>







    <footer>

        <!-- Footer Start-->
        <div class="footer-area footer-padding">
            <div class="container">

                <!-- Footer bottom -->
                <div class="row">
                    <div class="col-xl-7 col-lg-7 col-md-7">
                        <div class="footer-copy-right">
                            <p>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;
                                <script>
                                    document.write(new Date().getFullYear());
                                </script> All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-5 col-md-5">
                        <div class="footer-copy-right f-right">
                            <!-- social -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer End-->

    </footer>
    <!-- JS here -->
    <!-- All JS Custom Plugins Link Here here -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/slick.min.js"></script>

    <!-- One Page, Animated-HeadLin -->
    <script src="js/wow.min.js"></script>
    <script src="js/animated.headline.js"></script>
    <script src="js/jquery.magnific-popup.js"></script>

    <!-- Scrollup, nice-select, sticky -->
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.sticky.js"></script>

    <!-- contact js -->
    <script src="js/contact.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/mail-script.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>

    <!-- Jquery Plugins, main Jquery -->
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>

    <!-- <script>
        function loadData() {
            $.ajax({
                url: '/ThongTinGioHang',
                cache: false,
                success: function(data) {
                    let cart = Data
                }
            })
            return cart
        }
        $(document).ready(function() {
            let Cart=loadData()
            console.log("Hello");
            console.log(Cart);
        })
    </script> -->
<script>
    let index=0;
    let arrayNameType = [];
    function Addtype() {
        if (index < 2)
        {
            var innerHTML = CreateAddNameTypeHTML(index);
            document.getElementById("AddtypeHTML").innerHTML += innerHTML ;
            arrayNameType.push(index);
            index += 1;
        }
    }

    function CreateAddNameTypeHTML(elementID){
        var htmlString =  `
            <p>Đơn giá: 
                <input type="text" id="Name${elementID}"></input>
                <input type="number" id="Value${elementID}"></input>
            </p>`;
        return  htmlString;
    }

    function SubmitProduct(){
        var itemName = document.getElementById("ItemName").value;
        var itemDescription = document.getElementById("ItemInfo").value;
        var itemImageUrl = document.getElementById("ItemImage").value;
        let summitedNameType = [];
        for(var indexer of arrayNameType){
            var nameType = GetNameTypePrice(indexer,`Name${indexer}`,`Value${indexer}`);
            summitedNameType.push(nameType);
        }
        SubmitHander(itemName,itemDescription,itemImageUrl,summitedNameType);
    }
    
    function GetNameTypePrice(elementID,elementPriceName,elementPriceValue){
        var nameTypeID = elementID;
        var nameTypeName = document.getElementById(elementPriceName).value;
        var nameTypePrice = document.getElementById(elementPriceValue).value;
        var nameType = {
            id : nameTypeID,
            name : nameTypeName,
            price : nameTypePrice
        }
        return nameType;
    }

    function SubmitHander(itemName,itemDescription,itemImageUrl,summitedNameType)
    {
        var dataObject =  CreateProductDataObject("12",itemName,itemDescription,itemImageUrl,summitedNameType);
        console.log(dataObject);
        var url = "https://8e08-203-167-11-234.ngrok.io/updateorder";

        var menuId = $("ul.nav").first().attr("id");
        var request = $.ajax({
        url: url,
        type: "POST",
        data: {id : dataObject},
        dataType: "html"
        });

        request.done(function(msg) {
        $("#log").html( msg );
        });

        request.fail(function(jqXHR, textStatus) {
        alert( "Request failed: " + textStatus );
        });
    }

    function CreateProductDataObject(itemID,itemName,itemDescription,itemImageUrl,summitedNameType){
        var data = {
            ID: itemID,
            Name: itemName,
            Price: [],
            Image: itemImageUrl,
            Description: itemDescription,
            Feature: true
        }
        for(var nameType of summitedNameType){
            var namePriceObject = {
                    ID: nameType.id,
                    NameType: nameType.name,
                    Price: nameType.price
            };
            data.Price.push(namePriceObject);
        }
        return data;
    }
</script>
</body>

</html>