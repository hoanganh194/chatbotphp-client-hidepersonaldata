<!DOCTYPE html>
<html html class="no-js" lang="zxx">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Trang Nhân Viên Công Ty Sữa Hữu Cơ</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="/img/favicon.ico">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- CSS here -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/flaticon.css">
        <link rel="stylesheet" href="css/slicknav.css">
        <link rel="stylesheet" href="css/animate.min.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/fontawesome-all.min.css">
        <link rel="stylesheet" href="css/themify-icons.css">
        <link rel="stylesheet" href="css/slick.css">
        <link rel="stylesheet" href="css/nice-select.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <header>
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header ">
                <div class="header-top top-bg d-none d-lg-block">
                    <div class="container-fluid">
                        <div class="col-xl-12">
                            <div class="row d-flex justify-content-between align-items-center">
                                <div class="header-info-left d-flex">

                                    <!-- <div class="select-this">
                                        <form action="#">
                                            <div class="select-itms">
                                                <select name="sxelect" id="select1">
                                                    <option value="">Vietnam</option>
                                                </select>
                                            </div>
                                        </form>
                                    </div> -->
                                    <ul class="contact-now">
                                        <a href="/">
                                            <li>Công ty sữa hữu cơ Việt Nam</li>
                                        </a>
                                    </ul>
                                </div>
                                <div class="header-info-right">
                                    <ul>
                                        <li> <img src="/NhanVien/NV_2.png" alt="" style="width: 10%"><a href="/"> Xin chào: Tô thị bé Bé </a></li>
            <li><a href="/XLNhapThongTin/ThongTin"> Quản lý tài khoản</a></li>
            <li><a href="/XLLogOut">logout</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-bottom  header-sticky">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <!-- Logo -->
                            <div class="col-xl-1 col-lg-1 col-md-1 col-sm-3">

                            </div>
                            <div class="col-xl-6 col-lg-8 col-md-7 col-sm-5">
                                <!-- Main-menu -->
                                <div class="main-menu f-left d-none d-lg-block">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a href="/">Home</a></li>
            <li><a href="/ThemSanPham/ThongTin">Thêm sản phẩm mới</a></li>
            <li><a href="/DanhSachHoaDon">Danh sách hoá đơn</a></li>
            
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-xl-5 col-lg-3 col-md-3 col-sm-3 fix-card">
                                <ul class="header-right f-right d-none d-lg-block d-flex justify-content-between">
                                    <li class="d-none d-xl-block">
                                        <div class="form-box f-right ">
                                            <form action="/TimKiem" method="POST">
                    <input type="text" name="search" placeholder="Search products" value="">
                </form>
                                        </div>
                                    </li>
                                    
                                </ul>
                            </div>
                            <!-- Mobile Menu -->
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>
<main>
    <body>
        <div class="col-md-12 form-group p_star">
            <button class="btn_3" onclick="CreateMoreDirect()">Tạo Thêm Direct</button>
        </div>
        <div id=ScriptHTML></div>
        <div class="col-md-12 form-group"></div>
            <button class="btn_3" style="margin-left: 30rem;" onclick="Submit()">Submit</button>
        </div>
    </body>
</main>
    




<script>
    let DeletedDirect = [];
    let indexScript = 0;
let indexActionList = [];
let hashMap = [];
function CreateMoreDirect(){
    var scriptHTML = CreateConditionHTML(indexScript);
    indexActionList.push(0);
    var MainDiv = document.createElement('div');
    MainDiv.id = `MainDiv${indexScript}`;
    MainDiv.innerHTML = scriptHTML;
    document.getElementById("ScriptHTML").appendChild(MainDiv);
    indexOfAction=0;
    indexScript+=1;
}
function DeleteDirect(indexOfDirect){
    DeletedDirect.push(indexOfDirect);
    document.getElementById(`MainDiv${indexOfDirect}`).innerHTML = "";
}
function CreateConditionHTML(indexOfDirect){
    var htmlString = `
        <input type="text" style="margin-top:2rem" id="TextCheck${indexOfDirect}" placeholder="TextCheck"></input>
        <select id="TextCheckType${indexOfDirect}">
            <option value="-1">Không Giống</option>
            <option value="0">Không Kiểm Tra</option>
            <option value="1">Giống</option>
        </select>
        <button id="Delete${indexOfDirect}" onclick="DeleteDirect(${indexOfDirect})">Delete Direct</button>
        <br>
        <input type="text" class="" id="PayloadCheck${indexOfDirect}" placeholder="PayloadCheck"></input>
        <select id="PayloadCheckType${indexOfDirect}">
            <option value="-1">Không Giống</option>
            <option value="0">Không Kiểm Tra</option>
            <option value="1">Giống</option>
        </select>
        <br>
        <input type="text" class="" id="QuickMessageCheck${indexOfDirect}" placeholder="QuickMessageCheck"></input>
        <select id="QuickCheckType${indexOfDirect}">
            <option value="-1">Không Giống</option>
            <option value="0">Không Kiểm Tra</option>
            <option value="1">Giống</option>
        </select>
        <br>
        <button class="btn_4" onclick="CreateActionDetailHTML(${indexOfDirect})" id="ActionButton${indexOfDirect}">Thêm action</button>
        <div id="Action${indexOfDirect}"></div>
        `;
    return htmlString;
}
function CreateActionDetailHTML(indexScriptString){
    var indexScript = parseInt(indexScriptString);
    var indexAction = indexActionList[indexScript];
    var div = document.createElement('div');
    div.id = `ActionDetail${indexScript}-${indexAction}`;
    div.innerHTML = `
    <br>
    <select id="Type${indexScript}-${indexAction}" onchange="CheckAndCreateActionOption(${indexScript},${indexAction})">
                <option value="none">none</option>
                <option value="Send">Send</option>
                <option value="Show">Show</option>
                <option value="Update">Update</option>    
    </select>
    <div id= "optionValue${indexScript}-${indexAction}">
    </div>
    `;

    document.getElementById(`Action${indexScriptString}`).appendChild(div);
    indexActionList[indexScript]++;
}
function CheckAndCreateActionOption(indexScript,indexAction){
    var typeOption = document.getElementById(`Type${indexScript}-${indexAction}`).value;
    var innerHTML = "";
    switch(typeOption){
        case "Send":
            innerHTML = `
            <select id="Content${indexScript}-${indexAction}" onchange="CheckAndCreateSecondActionOption(${indexScript},${indexAction})">
                <option value="none">none</option>
                <option value="StructureMess">StructureMess</option>    
                <option value="NormalMess">NormalMess</option>  
                <option value="QuickMess">QuickMess</option> 
            </select>        
            <div id="OptionChosen${indexScript}-${indexAction}"></div>
            `
            break;
        case "Show":
            innerHTML = `
            <select id="Content${indexScript}-${indexAction}">
                <option value="FeatureProduct">FeatureProduct</option>
                <option value="SearchProduct">SearchProduct</option>
                <option value="ListNameType">ListNameType</option>
            </select>
            <input type="text" id="SubValue${indexScript}-${indexAction}" placeholder="SubValue"></input>  
            `
            break;
        case "Update":
            innerHTML = `
            <select id="Content${indexScript}-${indexAction}">
                <option value="Status">Status</option>
                <option value="Temp">Temp</option>
                <option value="Order">Order</option>
                <option value="Phone">Phone</option>
                <option value="Address">Address</option>
            </select>
            <input type="text" id="Value${indexScript}-${indexAction}" placeholder="Value"></input>
            <input type="text" id="SubValue${indexScript}-${indexAction}" placeholder="SubValue"></input>  
            `
            break;
    }
    document.getElementById(`optionValue${indexScript}-${indexAction}`).innerHTML = innerHTML;
}
function CheckAndCreateSecondActionOption(indexScript,indexAction)
{
    var keyword = `${indexScript}-${indexAction}`;
    var findValue = hashMap.find(x => x.key == keyword);
    console.log(findValue);
    console.log("Call");
    if(findValue != null){
        hashMap.pop(findValue);
        console.log("Call");
    }
    console.log(hashMap);
    var typeContent = document.getElementById(`Content${indexScript}-${indexAction}`).value;
    if(typeContent == "StructureMess")
    {
        var innerHTML=
        `
            <input type="text" id="Value${indexScript}-${indexAction}" placeholder="Value"></input>
            <input type="text" id="SubValue${indexScript}-${indexAction}" placeholder="SubValue"></input>
            <input type="text" id="DescValue${indexScript}-${indexAction}" placeholder="DescValue"></input>
            <input type="text" id="SubType${indexScript}-${indexAction}" placeholder="SubType"></input>
            <button onclick="OptionCreator(${indexScript},${indexAction})" class="btn-dark">Thêm Option</button>
            <div id="QuickMessOption${indexScript}-${indexAction}"></div>
        `
    }
    if(typeContent == "NormalMess")
    {
        var innerHTML=
        `
            <input type="text" id="Value${indexScript}-${indexAction}" placeholder="Value"></input>
        `
    }
    if(typeContent == "QuickMess")
    {
        var innerHTML=
        `
            <input type="text" id="Value${indexScript}-${indexAction}" placeholder="Value"></input>
            <button onclick="OptionCreator(${indexScript},${indexAction})" class="btn-dark">Thêm Option</button>
            <div id="QuickMessOption${indexScript}-${indexAction}"></div>
        `
    }
    document.getElementById(`OptionChosen${indexScript}-${indexAction}`).innerHTML=innerHTML;
}
function OptionCreator(indexScript,indexAction){
    var keyword = `${indexScript}-${indexAction}`;
    var findValue = hashMap.find(x => x.key == keyword);
    if(findValue == null){
        findValue = {
            key : `${indexScript}-${indexAction}`,
            value : 0
            };
        hashMap.push(findValue);
    }
    var div1 = document.createElement('div')
    div1.id = `QuickMessOptionDetail${indexScript}-${indexAction}`;
    div1.innerHTML = `
        <input id="Text${indexScript}-${indexAction}-${findValue.value}" placeholder="Text"></input>
        <input id="Payload${indexScript}-${indexAction}-${findValue.value}" placeholder="Payload"></input>
        <input id="ButtonType${indexScript}-${indexAction}-${findValue.value}" placeholder="ButtonType"></input>
        <br>    
    `
    findValue.value++;
    document.getElementById(`QuickMessOption${indexScript}-${indexAction}`).appendChild(div1);
}
function CreateNewScriptDetail(){
    var script = {
    TextCheck: "",
    TextCheckType: 0,
    PayloadCheck: "",
    PayloadCheckType: 0,
    QuickMessageCheck: "",
    QuickCheckType: 0,
    Action: []
    };
    return script;
}
function Submit(){
    var listScript = [];
    console.log(DeletedDirect);
    for(let i = 0; i < indexScript; i++) {
        if(DeletedDirect.find(x => x == i) == null)
        {
            var script = CreateScriptData(i);
            listScript.push(script);
        }
    }
    SubmitHander(listScript,DeletedDirect);
}
function CreateScriptData(index){
    var script = {
        TextCheck: "",
        TextCheckType: 0,
        PayloadCheck: "",
        PayloadCheckType: -1,
        QuickMessageCheck: "",
        QuickCheckType: 0,
        Action: []
    }
    script.TextCheck = document.getElementById(`TextCheck${index}`).value;
    script.TextCheckType = document.getElementById(`TextCheckType${index}`).value;
    script.PayloadCheck = document.getElementById(`PayloadCheck${index}`).value;
    script.PayloadCheckType = document.getElementById(`PayloadCheckType${index}`).value;
    script.QuickMessageCheck = document.getElementById(`QuickMessageCheck${index}`).value;
    script.QuickCheckType = document.getElementById(`QuickCheckType${index}`).value;
    for (let i = 0; i < indexActionList[index]; i++) {
        var action = CreateAndGetActionData(index,i);
        script.Action.push(action);
    }
    return script;
}

function CreateAndGetActionData(indexScriptAction,indexAction){
    var action = {
        Type: "",
        Content: "",
        Value: "",
        SubValue: "",
        DescValue: "",
        SubType: "",
        Option: []
    }
    action.Type = document.getElementById(`Type${indexScriptAction}-${indexAction}`).value;
    action.Content = document.getElementById(`Content${indexScriptAction}-${indexAction}`).value;
    var actionValue =  document.getElementById(`Value${indexScriptAction}-${indexAction}`);
    if(actionValue!= null){
        action.Value = actionValue.value
    }
    actionValue =  document.getElementById(`SubValue${indexScriptAction}-${indexAction}`);
    if(actionValue!= null){
        action.SubValue = actionValue.value
    }
    actionValue =  document.getElementById(`DescValue${indexScriptAction}-${indexAction}`);
    if(actionValue!= null){
        action.DescValue = actionValue.value
    }
    actionValue =  document.getElementById(`SubType${indexScriptAction}-${indexAction}`);
    if(actionValue!= null){
        action.SubType = actionValue.value
    }
    
    var key = `${indexScriptAction}-${indexAction}`;
    var findValue = hashMap.find(x => x.key == key);
    if(findValue != null){
        for (let i = 0; i < findValue.value; i++) {
            var button = CreateAndGetButtonData(indexScriptAction,indexAction,i);
            action.Option.push(button);
        }
    }
    
    return action;
}
function CreateAndGetButtonData(indexScriptAction, indexAction,indexButton){
    var button = {
        Text: "Mua hàng",
        Payload: "BUY",
        ButtonType: "Payload"
    }
    button.Text = document.getElementById(`Text${indexScriptAction}-${indexAction}-${indexButton}`).value;
    button.Payload = document.getElementById(`Payload${indexScriptAction}-${indexAction}-${indexButton}`).value;
    button.ButtonType = document.getElementById(`ButtonType${indexScriptAction}-${indexAction}-${indexButton}`).value;
    return button;
}
function SubmitHander(listScript)
{
    var ObjectScript = listScript;
    console.log(listScript);
    var url = "https://6e76-115-79-218-170.ngrok.io/UpdateOrder";
    var menuId = $("ul.nav").first().attr("id");
        
    var request = $.ajax({
        url: url,
        type: "POST",
        data: {id : ObjectScript},
        dataType: "html"
        });

        request.done(function(msg) {
        $("#log").html( msg );
        });

        request.fail(function(jqXHR, textStatus) {
        alert( "Request failed: " + textStatus );
        });
}
</script>
    <!-- All JS Custom Plugins Link Here here -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/slick.min.js"></script>

    <!-- One Page, Animated-HeadLin -->
    <script src="js/wow.min.js"></script>
    <script src="js/animated.headline.js"></script>
    <script src="js/jquery.magnific-popup.js"></script>

    <!-- Scrollup, nice-select, sticky -->
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.sticky.js"></script>

    <!-- contact js -->
    <script src="js/contact.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/mail-script.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>

    <!-- Jquery Plugins, main Jquery -->
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>