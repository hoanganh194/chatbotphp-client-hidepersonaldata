<?php
session_start();
if (file_exists(__DIR__ . '/config.php')) {
    $config = include __DIR__ . '/config.php';
}

session_destroy();
header('Location: ' .  $config['url']. '/dangnhap.php');
exit;

?>