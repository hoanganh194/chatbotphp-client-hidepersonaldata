<?php
if(!session_id()) {
  session_start();
}
if (file_exists(__DIR__ . '/config.php')) {
  $config = include __DIR__ . '/config.php';
}
require_once( 'Facebook/autoload.php' );
$fb = new Facebook\Facebook([
  'app_id' => $config['app_id'],
  'app_secret' =>  $config['app_secret'],
  'default_graph_version' => 'v2.9',
  ]);

$helper = $fb->getRedirectLoginHelper();
$loginUrl = $helper->getLoginUrl($config['url'].'/fb-callback.php', $config['permissions']);
echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';
?>